/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simpleclient;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

/**
 *
 * @author pfb1
 */
public class SimpleClient {
    
    Socket socket = null;
    PrintWriter out = null;
    Scanner in = null;
    Scanner keyboard = null;
    
    public SimpleClient()
    {
        keyboard = new Scanner(System.in);
    }
    
    public Boolean Connect(String hostname, int port)
    {
        try
        {
            socket = new Socket(hostname, port);
            out = new PrintWriter(socket.getOutputStream(), true);
            in = new Scanner(socket.getInputStream());
        }
        catch (Exception e)
        {
            System.out.println("Exception" + e.getMessage());
            return false;
        }
        
        return true;
    }
    
    public void Run() throws NotConnectedException, IOException
    {
        if (!socket.isConnected()) 
        {
            throw new NotConnectedException();
        }
        
        try
        {
            String userInput;
            
            ProcessServerResponse();
            
            System.out.print("Enter the data to be sent: ");
            
            while ((userInput = keyboard.nextLine()) != null) 
            {
                out.println(userInput);
                ProcessServerResponse();
            
                if (userInput.equalsIgnoreCase("9")) 
                {
                    break;
                }
                
                System.out.print("Enter the data to be sent: ");
            }
        }
        catch (Exception e)
        {
            System.out.println("Unexpected Error: " + e.getMessage());
        }
        finally
        {
            out.close();
            in.close();
            keyboard.close();
            socket.close();
        }
    }
    
    private void ProcessServerResponse()
    {
        String response = in.nextLine();
	System.out.println("Server says: " + response);
    }
}
