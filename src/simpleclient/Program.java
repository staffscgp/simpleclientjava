/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simpleclient;

/**
 *
 * @author pfb1
 */
public class Program {

    private static final String hostname = "127.0.0.1";
    private static final int port = 4444;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
        SimpleClient client = new SimpleClient();
        
        if (client.Connect(hostname, port))
        {
            System.out.println("Connected...");
            
            try
            {
                client.Run();
            }
            catch (NotConnectedException e)
            {
                System.out.println(e.getMessage());
            }
            catch (Exception e)
            {
                System.out.println("Unexpected Exception: " + e.getMessage());
            }
        }
        else
        {
            System.out.println("Failed to connect to: " + hostname + ":" + port);
        }
    }
}
